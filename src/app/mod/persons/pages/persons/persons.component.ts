import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatSort, MatTableDataSource, Sort} from '@angular/material';
import {Person} from "../../shared/persons";
import {PersonsService} from "../../shared/persons.service";
import {PersonDetailsComponent} from "../../components/person-details/person-details.component";


@Component({
    selector: 'sy-persons',
    templateUrl: './persons.component.html',
    styleUrls: ['./persons.component.scss']
})
export class PersonsComponent implements OnInit {

    @ViewChild(MatSort) sort: MatSort;

    persons: Person[];
    displayedColumns = ['login', 'name', 'lastupdate', 'actions'];
    dataSource: MatTableDataSource<any> = new MatTableDataSource([]);
    sortActive = '';
    sortDirection = '';

    constructor(
        private personsService: PersonsService,
        public dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

    }

    ngOnInit() {
        this.dataSource.sort = this.sort;
        this.loadData();

        setInterval(() => {
            this.loadData();
        }, 30000);
    }

    private loadData() {
        this.personsService.getAll()
            .subscribe(data => {
                this.persons = data;
                this.sorting();
            });
    }


    addDevice() {
        event.stopPropagation();
        const dialogRef = this.dialog.open(PersonDetailsComponent, <MatDialogConfig>{
            width: '70%',
            data: {
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadData();
            }
        });
    }

    editRow(row: Person , event) {
        event.stopPropagation();
        const dialogRef = this.dialog.open(PersonDetailsComponent, <MatDialogConfig>{
            width: '70%',
            data: {
                row: row
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadData();
            }
        });
    }

    sortData(sort: Sort) {
        if (!sort.active || sort.direction === '') {
            return;
        }
        this.sortActive = sort.active;
        this.sortDirection = sort.direction;
        this.sorting();
    }

    sorting() {
        const isAsc = this.sortDirection === 'asc';

        this.dataSource.data = this.persons.sort((a, b) => {
            switch (this.sortActive) {
                /*case 'id': return this.compare(a.device.deviceId, b.device.deviceId, isAsc);
                case 'ipaddress': return this.compareIpAddress(a.device.ipAddress, b.device.ipAddress) * (isAsc ? 1 : -1);
                case 'macaddress': return this.compare(a.device.mac, b.device.mac, isAsc);*/
                default: return 0;
            }
        });
    }

    compare(a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }
}
