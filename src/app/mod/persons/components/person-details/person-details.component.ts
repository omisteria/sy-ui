import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs';
import {DeviceObserver, Person} from "../../shared/persons";
import {PersonsService} from "../../shared/persons.service";


@Component({
    selector: 'sy-person-details',
    templateUrl: './person-details.component.html',
    styleUrls: ['./person-details.component.scss']
})
export class PersonDetailsComponent {

    formGroup: FormGroup;
    availablePlatforms: Observable<any>;
    person: Person;
    devices: DeviceObserver[];
    isNew = false;
    deviceIds: string[];

    constructor(
        public fb: FormBuilder,
        private snackBar: MatSnackBar,
        private personsService: PersonsService,
        public dialogRef: MatDialogRef<PersonDetailsComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

        this.person = this.data.row;

        if (this.person) {
            this.deviceIds = this.person.observedDevices.map(d => d.deviceId);
            this.formGroup = fb.group({
                'login': [this.person.login, [Validators.required, Validators.minLength(4)]],
                'name': [this.person.name, [Validators.required, Validators.minLength(11)]],
                'deviceIds': [this.deviceIds, []]
            });
        } else {
            this.person = new Person();
            this.isNew = true;
            this.formGroup = fb.group({
                'login': [this.person.login, [Validators.required, Validators.minLength(4)]],
                'name': [this.person.name, [Validators.required, Validators.minLength(11)]],
                'deviceIds': [this.deviceIds, []]
            });
        }

        personsService.getAllDevices().subscribe(d => {
            this.devices = d;
        });
    }

    save() {
        if (this.formGroup.valid) {

            this.person.login = this.formGroup.get('login').value;
            this.person.name = this.formGroup.get('name').value;
            const deviceIds = this.formGroup.get('deviceIds').value || [];
            this.person.observedDevices = deviceIds.map(d => {
                return {deviceId: d, active: false}
            });


            if (this.isNew) {
                this.personsService.add(this.person)
                    .subscribe(data => {
                        this.dialogRef.close(data);
                    });
            } else {
                this.personsService.update(this.person)
                    .subscribe(data => {
                        this.dialogRef.close(data);
                    });
            }
        }
    }

    discard() {
        this.dialogRef.close();
    }
}
