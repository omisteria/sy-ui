import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {PersonsComponent} from "./pages/persons/persons.component";
import {OrderByPipe} from "./components/order-by-pipe";
import {PersonDetailsComponent} from "./components/person-details/person-details.component";
import {PersonsService} from "./shared/persons.service";
import {SharedModule} from "../shared.module";
import {MaterialModule} from "../material.module";


const routes: Routes = [
    {
        path: 'persons',
        pathMatch: 'full',
        redirectTo: 'persons/list'
    },
    {
        path     : 'persons/list',
        component: PersonsComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        SharedModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        OrderByPipe,
        PersonDetailsComponent,
      PersonsComponent
    ],
    exports: [
        MaterialModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        PersonDetailsComponent
    ],
    providers: [
        PersonsService,
        {provide: MatDialogRef, useValue: {}},
        {provide: MAT_DIALOG_DATA, useValue: {}}
    ],
    entryComponents: [
        PersonDetailsComponent
    ]
})
export class PersonsModule {

}
