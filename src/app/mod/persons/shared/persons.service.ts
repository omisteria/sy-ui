import {Injectable} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from "../../../../environments/environment";
import {DeviceObserver, Person} from "./persons";
import {catchError} from "rxjs/internal/operators";



@Injectable()
export class PersonsService {

    private baseURL;

    constructor(private http: HttpClient) {
        this.baseURL = environment.baseURL + '/persons/api';
    }

    getAllDevices(): Observable<DeviceObserver[]> {
        return this.http.get<DeviceObserver[]>(environment.baseURL + '/devmonitor/observers/all')
          .pipe(catchError(this.handleError));
    }

    getAll(): Observable<Person[]> {
        return this.http.get<Person[]>(this.baseURL + '/list/all')
          .pipe(catchError(this.handleError));
    }

    add(person: Person): Observable<Person> {
        return this.http.post<Person>(this.baseURL + '/add', person)
          .pipe(catchError(this.handleError));
    }

    update(person: Person): Observable<Person> {
        return this.http.put<Person>(this.baseURL + '/update', person)
          .pipe(catchError(this.handleError));
    }

    /*delete(person: Person): Observable<Person> {
        return this.http.delete<Person>(this.baseURL + '/delete', person)
            .catch(this.handleError);
    }*/

    private handleError(err: HttpErrorResponse) {
        if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', err.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            const et = JSON.stringify(err.error);
            console.error(`Backend returned code ${err.status}, body was: ${et}`);
        }
        return throwError(err);
    }
}
