export class Person {
    id: number;
    name: string;
    login: string;
    active: boolean;
    observedDevices: any[];
    regDate: Date;
}

export class Device {
    deviceId: string;
    description: string;
    ipAddress: string;
    mac: string;
    type: string;
}

export class DeviceObserver {
    device: Device;
    available: boolean;
    timeStatusChange: number;
}
