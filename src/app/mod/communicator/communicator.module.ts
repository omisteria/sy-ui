import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from "@angular/router";
import {ChannelsComponent} from "./pages/channels/channels.component";
import {CommunicatorService} from "./shared/communicator.service";
import {SharedModule} from "../shared.module";
import {MaterialModule} from "../material.module";

const routes: Routes = [
    {
        path: 'communicator',
        pathMatch: 'full',
        redirectTo: 'communicator/list'
    },
    {
        path     : 'communicator/list',
        component: ChannelsComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        SharedModule,
        MaterialModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
      ChannelsComponent
    ],
    exports: [
        MaterialModule,
        SharedModule
    ],
    providers: [
        CommunicatorService
    ]
})
export class CommunicatorModule {

}
