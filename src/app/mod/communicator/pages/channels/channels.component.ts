import { Component, OnInit } from '@angular/core';
import {MatSnackBar, MatSnackBarConfig, MatTableDataSource} from '@angular/material';
import {ProcessDefinition} from "../../shared/communicator";
import {CommunicatorService} from "../../shared/communicator.service";



@Component({
    selector: 'sy-channels',
    templateUrl: './channels.component.html',
    styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit {

    processDefinitions: ProcessDefinition[];

    dataSource: MatTableDataSource<ProcessDefinition> = new MatTableDataSource([]);
    displayedColumns = ['icon', 'description', 'triggered', 'actions'];

    constructor(
        private communicatorService: CommunicatorService,
        public snackBar: MatSnackBar) {

    }

    ngOnInit(): void {
        setInterval(() => this.loadData(), 6000);
        this.loadData();
    }

    loadData() {
        this.communicatorService.getProcessDefinitions()
            .subscribe(
                data => {
                    this.processDefinitions = data;
                    this.dataSource.data = data;
                }
            );
    }

    invokeCheck(processDefinition: ProcessDefinition) {
        this.communicatorService
            .runCheckProcess(processDefinition.id)
            .subscribe(
                data => {
                    console.log(data);
                    this.processDefinitions
                        .filter(pd => pd.id === data.id)
                        .forEach(pd => pd.status = data.status);
                    this.dataSource.data = this.processDefinitions;

                    this.snackBar.open('Runned', 'Success', <MatSnackBarConfig>{
                        duration: 2000
                    });
                },
                error => {
                    console.error(error);
                    this.snackBar.open(error, 'Error', <MatSnackBarConfig>{
                        duration: 2000
                    });
                }
            );
    }

}
