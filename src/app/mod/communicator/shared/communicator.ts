export class ProcessDefinition {
    className: string;
    description: string;
    events: string[];
    health: number;
    id: number;
    triggered: Date;
    status: string;
}