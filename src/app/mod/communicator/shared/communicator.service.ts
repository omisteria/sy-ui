import { Injectable } from '@angular/core';
import {Observable, Subject, throwError} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ProcessDefinition} from "./communicator";
import {catchError} from "rxjs/internal/operators";



@Injectable()
export class CommunicatorService {

    baseURL: string;

    constructor(private http: HttpClient) {
        this.baseURL = environment.baseURL + '/communicator/api';
    }

    getProcessDefinitions(): Observable<ProcessDefinition[]> {
        const url = `${this.baseURL}/processDefinitions`;
        return this.http
            .get<ProcessDefinition[]>(url)
            .pipe(catchError(this.handleError));
    }

    runCheckProcess(pdId: number): Observable<ProcessDefinition> {
        const url = `${this.baseURL}/runCheck/${pdId}`;
        return this.http
          .post<ProcessDefinition>(url, {})
          .pipe(catchError(this.handleError));
    }

    private handleError(err: HttpErrorResponse) {
        if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', err.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            let et = JSON.stringify(err.error);
            console.error(`Backend returned code ${err.status}, body was: ${et}`);
        }
        return throwError(err);
    }
}
