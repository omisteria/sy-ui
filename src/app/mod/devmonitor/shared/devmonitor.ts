export class Device {
  deviceId: string;
  description: string;
  ipAddress: string;
  mac: string;
  type: string;
}

export class DeviceObserver {
  device: Device;
  available: boolean;
  timeStatusChange: number;
}

export class DevicePage {
  content: Device[];
  totalElements: number;
}
