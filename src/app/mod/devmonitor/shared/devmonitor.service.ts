import {Injectable} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from "../../../../environments/environment";
import {Device, DeviceObserver} from "./devmonitor";
import {catchError} from "rxjs/internal/operators";
import { of } from 'rxjs'


@Injectable()
export class DevMonitorService {

    private baseURL;

    constructor(private http: HttpClient) {
        this.baseURL = environment.baseURL + '/devmonitor/api';
    }

    arrayPlatforms(): Observable<any> {
        return of([
            {id: 'android', name: 'Android', icon: 'android'},
            {id: 'mac', name: 'OSX', icon: 'desktop_mac'},
            {id: 'windows-mobile', name: 'Windows Mobile', icon: 'windows'},
            {id: 'windows', name: 'Windows', icon: 'desktop_windows'},
            {id: 'linux', name: 'Linux', icon: 'pets'}
        ]);
    }

    getAllDevices(): Observable<DeviceObserver[]> {
        return this.http.get<DeviceObserver[]>(this.baseURL + '/observers/all')
            .pipe(catchError(this.handleError));
    }

    saveDevice(device: Device): Observable<Device> {
        return this.http.post<Device>(this.baseURL + '/observer', device)
            .pipe(catchError(this.handleError));
    }

    deleteDevice(device: Device): Observable<Device> {
        return this.http.put<Device>(this.baseURL + '/observer/' + device.deviceId, device)
            .pipe(catchError(this.handleError));
    }

    private handleError(err: HttpErrorResponse) {
        if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', err.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            const et = JSON.stringify(err.error);
            console.error(`Backend returned code ${err.status}, body was: ${et}`);
        }
        return throwError(err);
    }
}
