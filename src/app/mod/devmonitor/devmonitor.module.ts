import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {DevicesComponent} from "./pages/devices/devices.component";
import {DevMonitorService} from "./shared/devmonitor.service";
import {PlatformComponent} from "./components/platform/platform.component";
import {DeviceDetailsComponent} from "./components/device-details/device-details.component";
import {SharedModule} from "../shared.module";
import {MaterialModule} from "../material.module";

const routes: Routes = [
    {
        path: 'devmonitor',
        pathMatch: 'full',
        redirectTo: 'devmonitor/list'
    },
    {
        path     : 'devmonitor/list',
        component: DevicesComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        SharedModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        DeviceDetailsComponent,
        PlatformComponent,
      DevicesComponent
    ],
    exports: [
        MaterialModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        DeviceDetailsComponent,
        PlatformComponent
    ],
    providers: [
        DevMonitorService,
        {provide: MatDialogRef, useValue: {}},
        {provide: MAT_DIALOG_DATA, useValue: {}}
    ],
    entryComponents: [
        DeviceDetailsComponent
    ]
})
export class DevMonitorModule {

}
