import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatSort, MatTableDataSource, Sort} from '@angular/material';
import {DevMonitorService} from "../../shared/devmonitor.service";
import {DeviceDetailsComponent} from "../../components/device-details/device-details.component";
import {DeviceObserver} from "../../shared/devmonitor";


@Component({
    selector: 'sy-monitor-devices',
    templateUrl: './devices.component.html',
    styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit {

    @ViewChild(MatSort) sort: MatSort;

    devices: DeviceObserver[];
    displayedColumns = ['id', 'description', 'ipaddress', 'macaddress', 'lastupdate', 'actions'];
    dataSource: MatTableDataSource<any> = new MatTableDataSource([]);
    sortActive = '';
    sortDirection = '';

    constructor(
        private devmonitorService: DevMonitorService,
        public dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

    }

    ngOnInit() {
        this.dataSource.sort = this.sort;
        this.loadData();

        setInterval(() => {
            this.loadData();
        }, 30000);
    }

    private loadData() {
        this.devmonitorService.getAllDevices()
            .subscribe(data => {
                this.devices = data;
                this.sorting();
            });
    }


    addDevice() {
        event.stopPropagation();
        const dialogRef = this.dialog.open(DeviceDetailsComponent, <MatDialogConfig>{
            width: '70%',
            data: {
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadData();
            }
        });
    }

    editRow(row: DeviceObserver , event) {
        event.stopPropagation();
        const dialogRef = this.dialog.open(DeviceDetailsComponent, <MatDialogConfig>{
            width: '70%',
            data: {
                row: row.device
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadData();
            }
        });
    }

    sortData(sort: Sort) {
        if (!sort.active || sort.direction === '') {
            return;
        }
        this.sortActive = sort.active;
        this.sortDirection = sort.direction;
        this.sorting();
    }

    sorting() {
        const isAsc = this.sortDirection === 'asc';

        this.dataSource.data = this.devices.sort((a, b) => {
            switch (this.sortActive) {
                case 'id': return this.compare(a.device.deviceId, b.device.deviceId, isAsc);
                case 'ipaddress': return this.compareIpAddress(a.device.ipAddress, b.device.ipAddress) * (isAsc ? 1 : -1);
                case 'macaddress': return this.compare(a.device.mac, b.device.mac, isAsc);
                default: return 0;
            }
        });
    }

    compare(a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    compareIpAddress(a, b) {
        a = a.split( '.' ).map(e => parseInt(e));
        b = b.split( '.' ).map(e => parseInt(e));
        for (let i = 0; i < a.length; i++ ) {
            if (a[i] < b[i]) {
                return -1;
            } else if (a[i] > b[i]) {
                return 1;
            }
        }
        return 0;
    }

    click1() {
        /**/
    }
    click2() {
        console.log('click 2');
    }
    click3() {
        console.log('click 3');
    }
}
