import { Component, OnInit } from '@angular/core';
import {StickerService} from "../../shared/sticker.service";
import {Sticker} from "../../shared/sticker";

@Component({
  selector: 'sy-sticker-fav-services',
  templateUrl: './sticker-fav-services.component.html',
  styleUrls: ['./sticker-fav-services.component.scss']
})
export class StickerFavServicesComponent implements OnInit {

  favorites: Sticker[];

  constructor(private stickerService: StickerService) { }

  ngOnInit() {

    this.stickerService.fetchFavStickers()
      .subscribe(stickers => {
        this.favorites = stickers;
      });
  }

  openService(sticker, event) {
    //event.stopPropagation();
    window.open(sticker.baseURL);
  }

}
