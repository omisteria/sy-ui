import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material";
import {StickerService} from "../../shared/sticker.service";
import {StickerDetails} from "../../shared/sticker";

@Component({
    selector: 'sy-sticker-app-mumen-dlg',
    templateUrl: './sticker-app-mumen-dlg.component.html',
    styleUrls: ['./sticker-app-mumen-dlg.component.scss']
})
export class StickerAppMumenDialogComponent implements OnInit {

  data: StickerDetails;

    constructor(
      private stickerService: StickerService,
      @Inject(MAT_DIALOG_DATA) public dialogData: any
    ) {

    }

    ngOnInit() {
      this.stickerService.fetchStickerDetails(this.dialogData.stickerId)
        .subscribe(d => {
          this.data = d;
        });
    }

}
