import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {StickerService} from "../../shared/sticker.service";
import {Sticker} from "../../shared/sticker";


@Component({
    selector: 'app-sticker-det',
    templateUrl: './sticker-det.component.html',
    styleUrls: ['./sticker-det.component.scss']
})
export class StickerDetComponent implements OnInit {

    formGroup: FormGroup;

    constructor(
        public fb: FormBuilder,
        private snackBar: MatSnackBar,
        private apiCommonService: StickerService,
        public dialogRef: MatDialogRef<StickerDetComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.formGroup = fb.group({
            'title': ['', [Validators.required, Validators.minLength(4)]],
            'description': ['', [Validators.required, Validators.minLength(4)]],
            'url': ['', [Validators.required, Validators.minLength(4)]]
        });
    }

    ngOnInit() {
    }

    save() {
        if (this.formGroup.valid) {
            const sticker: Sticker = new Sticker();
            sticker.type = 'link/bookmark';
            sticker.title = this.formGroup.get('title').value;
            sticker.description = this.formGroup.get('description').value;
            sticker.baseURL = this.formGroup.get('url').value;

            this.apiCommonService.saveBookmark(sticker)
                .subscribe(data => {
                    this.dialogRef.close(data);
                });
        }
    }

    discard() {
        this.dialogRef.close();
    }
}
