import {Component, Inject, Input, OnInit} from '@angular/core';
import {Sticker, StickerDetails} from "../../shared/sticker";
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig} from "@angular/material";
import {StickerAppMumenDialogComponent} from "../sticker-app-mumen-dlg/sticker-app-mumen-dlg.component";
import {StickerService} from "../../shared/sticker.service";

@Component({
    selector: 'sy-sticker-bookmark',
    templateUrl: './sticker-bookmark.component.html',
    styleUrls: ['./sticker-bookmark.component.scss']
})
export class StickerBookmarkComponent implements OnInit {

    @Input()
    sticker: Sticker;


    constructor(
      private stickerService: StickerService
    ) {

    }

    ngOnInit() {


    }

  openService(event) {
    event.stopPropagation();
    window.open(this.sticker.baseURL);
  }

  favorite() {
    this.stickerService.markFavorite(this.sticker.id, true)
      .subscribe(() => {
        //this.loadFavoriteStickers();
        //this.loadAllStickers();
      });
  }

}
