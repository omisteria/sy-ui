import {Component, Inject, Input, OnInit} from '@angular/core';
import {HealthInfo} from "../../shared/sticker";

@Component({
    selector: 'app-health-status',
    templateUrl: './health-status.component.html',
    styleUrls: ['./health-status.component.scss']
})
export class HealthStatusComponent implements OnInit {

    @Input()
    healthInfo: HealthInfo;

    constructor(
    ) {

    }

    ngOnInit() {
    }

}
