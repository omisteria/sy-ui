import {Component, OnInit, Input} from '@angular/core';
import {Sticker} from "../../shared/sticker";


@Component({
  selector: 'app-morph-block',
  templateUrl: './morph-block.component.html',
  styleUrls: ['./morph-block.component.scss']
})
export class MorphBlockComponent implements OnInit {

  @Input() sticker: Sticker;

  isOpened:boolean = false;

  constructor() { }

  ngOnInit() {
  }

  toggleOpen() {
    this.isOpened = !this.isOpened;
  }

  openService(event) {
    event.stopPropagation();
    window.open(this.sticker.baseURL);
  }
}
