import {Component, Inject, Input, OnInit} from '@angular/core';
import {Sticker, StickerDetails} from "../../shared/sticker";
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig} from "@angular/material";
import {StickerAppMumenDialogComponent} from "../sticker-app-mumen-dlg/sticker-app-mumen-dlg.component";
import {StickerService} from "../../shared/sticker.service";

@Component({
    selector: 'sy-sticker-app-things',
    templateUrl: './sticker-app-things.component.html',
    styleUrls: ['./sticker-app-things.component.scss']
})
export class StickerAppThingsComponent implements OnInit {

    @Input()
    sticker: Sticker;

  details: StickerDetails;

  showContent = false;

    constructor(
      private stickerService: StickerService,
      public dialog: MatDialog
    ) {

    }

    ngOnInit() {


    }

  toggleContent() {
      this.showContent = !this.showContent;
      if (this.showContent) {
        this.getDetails();
      }
  }

  openService(event) {
    event.stopPropagation();
    window.open(this.sticker.baseURL);
  }

  openDialog() {
    const dialogRef = this.dialog.open(StickerAppMumenDialogComponent, <MatDialogConfig>{
      width: '98%',
      maxWidth: 'auto',
      minHeight: '400px',
      data: {
        stickerId: this.sticker.name
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }

  private getDetails() {
    this.stickerService.fetchStickerDetails(this.sticker.name)
      .subscribe(d => {
        this.details = d;
      });
  }
}
