import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig, MatTableDataSource} from '@angular/material';
import {Sticker} from "../../shared/sticker";
import {StickerService} from "../../shared/sticker.service";
import {StickerDetComponent} from "../../components/sticker-det/sticker-det.component";


@Component({
    selector: 'sy-stickers',
    templateUrl: './stickers.component.html',
    styleUrls: ['./stickers.component.scss']
})
export class StickersComponent implements OnInit {

    favorites: Sticker[];
    stickers: Sticker[];
    displayedColumns = ['icon', 'title', 'author', 'description', 'favorite'];
    favDataSource: MatTableDataSource<any> = new MatTableDataSource([]);
    dataSource: MatTableDataSource<any> = new MatTableDataSource([]);

    pageTotal;
    pageIndex = 0;
    pageSize = 20;

    constructor(
        private apiCommonService: StickerService,
        public dialog: MatDialog
    ) {

    }

    ngOnInit() {
        /*this.apiCommonService.searchTextAnnouncedSource.subscribe(
            term => {
                this.apiCommonService.searchStickers(term)
                    .subscribe(stickers => {
                        this.stickers = stickers;
                        this.dataSource.data = stickers.content;
                    });
            });*/

        this.loadFavoriteStickers();
        this.loadAllStickers();

        /*setInterval(() => {
            this.loadFavoriteStickers();
            this.loadAllStickers();
        }, 30000);*/
    }

    private loadAllStickers() {
        this.apiCommonService.fetchRecentStickers(this.pageIndex, this.pageSize)
            .subscribe(data => {
                this.stickers = data.content;
                this.dataSource.data = this.stickers;

                this.pageTotal = data.totalElements;
            });
    }

    private loadFavoriteStickers() {
        this.apiCommonService.fetchFavStickers()
            .subscribe(stickers => {
                this.favorites = stickers;
                this.favDataSource.data = stickers;
            });
    }

    openService(sticker, event) {
        event.stopPropagation();
        window.open(sticker.baseURL);
    }

    removeFromFav(sticker: Sticker) {
        this.apiCommonService.markFavorite(sticker.id, false)
            .subscribe(() => {
                this.loadFavoriteStickers();
                this.loadAllStickers();
            });
    }
    addToFav(sticker: Sticker) {
        this.apiCommonService.markFavorite(sticker.id, true)
            .subscribe(() => {
                this.loadFavoriteStickers();
                this.loadAllStickers();
            });
    }

    changeAllStickersPage(pager) {
        this.pageIndex = pager.pageIndex ;
        this.loadAllStickers();
    }

    click1() {
        const dialogRef = this.dialog.open(StickerDetComponent, <MatDialogConfig>{
            width: '70%',
            data: {
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadAllStickers();
            }
        });
    }
    click2() {
        console.log('click 2');
    }
    click3() {
        console.log('click 3');
    }
}
