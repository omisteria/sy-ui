import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {StickersComponent} from "./pages/stickers/stickers.component";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HealthStatusComponent} from "./components/health-status/health-status.component";
import {MorphBlockComponent} from "./components/morph-block/morph-block.component";
import {StickerDetComponent} from "./components/sticker-det/sticker-det.component";
import {StickerService} from "./shared/sticker.service";
import { StickerFavServicesComponent } from './components/sticker-fav-services/sticker-fav-services.component';
import {SharedModule} from "../shared.module";
import {StickerAppMumenComponent} from "./components/sticker-app-mumen/sticker-app-mumen.component";
import {StickerAppMumenDialogComponent} from "./components/sticker-app-mumen-dlg/sticker-app-mumen-dlg.component";
import {StickerBookmarkComponent} from "./components/sticker-bookmark/sticker-bookmark.component";
import {StickerAppThingsComponent} from "./components/sticker-app-things/sticker-app-things.component";

const routes: Routes = [
  {
    path: 'sticker',
    pathMatch: 'full',
    redirectTo: '/sticker/list'
  },
  {
    path     : 'sticker/list',
    component: StickersComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    StickerFavServicesComponent
  ],
  declarations: [
    StickersComponent,
    HealthStatusComponent,
    MorphBlockComponent,
    StickerDetComponent,
    StickerFavServicesComponent,
    StickerBookmarkComponent,
    StickerAppMumenComponent,
    StickerAppThingsComponent,
    StickerAppMumenDialogComponent
  ],
  providers: [
    FormBuilder,
    StickerService
  ],
  entryComponents: [
    StickerAppMumenDialogComponent
  ]
})
export class StickerModule { }
