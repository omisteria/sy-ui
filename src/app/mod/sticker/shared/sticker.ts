export class Sticker {
    id:number;
    name:string;
    title:string;
    baseURL:string;
    description:string;
    icon:string;
    type:string;
    author:string;
    publishDate:string;
    favorite: boolean;
    healthInfo: HealthInfo;

    constructor() {

    }
}
export class StickerItem {
  id: string;
  lastUpdate: number;
  stickerId: string;
  thingId: string;
  type: string;
  description: string;
  value: string;
}
export class StickerDetails {
  items: StickerItem[];
}

export class StickerPage {
    content: Sticker[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort: any;
    totalElements: number;
    totalPages: number;
}

export class DiskSpaceInfo {
    status: string;
    total: number;
    free: number;
    threshold: number;
}
export class DatabaseInfo {
    status: string;
    database: string;
    hello: number;
}
export class HealthInfo {
    status: string;
    diskSpace: DiskSpaceInfo;
    db: DatabaseInfo;
}
