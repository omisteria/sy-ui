import { Injectable } from '@angular/core';
import {Observable, Subject, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Sticker, StickerDetails, StickerPage} from "./sticker";
import {catchError} from "rxjs/internal/operators";


@Injectable()
export class StickerService {

  // Observable string sources
  public searchTextAnnouncedSource: Subject<string> = new Subject<string>();
  //private searchTextConfirmedSource = new Subject<string>();

  // Observable string streams
  //searchTextAnnounced$ = this.searchTextAnnouncedSource.asObservable();
  //searchTextConfirmed$ = this.searchTextConfirmedSource.asObservable();

  // Service message commands
  announceSearchText(text: string) {
    this.searchTextAnnouncedSource.next(text);
  }

  //confirmSearchText(backString: string) {
  //  this.searchTextConfirmedSource.next(backString);
  //}
  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.baseURL + '/sticker/api';
  }

  extractData(res: Response) {
    /*var descs = res.json().content;
    for (var i = 0; i < descs.length; i++) {
        this.syServices.push(new SyServiceDesc(descs[i]));
    }*/
    return res.json();
  }

  handleError(error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return throwError(errMsg);
  }

  public searchStickers(searchText: string): Observable<StickerPage> {
    return this.http.get<StickerPage>(this.baseURL + '/search?r=' + searchText)
      .pipe(catchError(this.handleError));
  }

  public fetchFavStickers(): Observable<Sticker[]> {
    return this.http.get<Sticker[]>(this.baseURL + '/favorites')
      .pipe(catchError(this.handleError));
  }

  public fetchStickerDetails(stickerId: string): Observable<StickerDetails> {
    const url = `${this.baseURL}/sticker/${stickerId}/details`;
    return this.http.get<StickerDetails>(url)
      .pipe(catchError(this.handleError));
  }

  public fetchRecentStickers(pageIndex: number, pageSize: number): Observable<StickerPage> {
    return this.http.get<StickerPage>(this.baseURL + `/recent?page=${pageIndex}&rows=${pageSize}`)
      .pipe(catchError(this.handleError));
  }

  public markFavorite(stickerId: number, flag: boolean): Observable<StickerPage> {
    return this.http.post<StickerPage>(this.baseURL + `/${stickerId}/favorite?flag=${flag}`, {})
      .pipe(catchError(this.handleError));
  }

  public saveBookmark(bookmark: Sticker): Observable<Sticker> {
    return this.http.post<Sticker>(this.baseURL + `/save`, bookmark)
      .pipe(catchError(this.handleError));
  }

}
