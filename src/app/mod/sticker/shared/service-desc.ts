export class ServiceDesc {
  id:string;
  name:string;
  landingPage:string;
  description:string;
  icon:string;

  private desc;

  constructor(desc) {
    this.desc = desc;

    this.id = desc.id;
    this.name = desc.name;
    this.landingPage = desc.landingPage;
    this.description = desc.description;
    this.icon = desc.icons.d48;
  }
}
