import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from "@angular/common/http";
import {MaterialModule} from "./material.module";
import {FormBuilder, FormsModule} from '@angular/forms';
import {FlexLayoutModule} from "@angular/flex-layout";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        MaterialModule,
      FlexLayoutModule
    ],
    declarations: [

    ],
    exports: [
        FormsModule,
        MaterialModule,
      FlexLayoutModule
    ],
    providers: [
        FormBuilder
    ]
})
export class SharedModule { }
