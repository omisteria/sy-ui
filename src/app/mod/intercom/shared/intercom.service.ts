import { Injectable } from '@angular/core';
import {Observable, throwError} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {catchError} from "rxjs/internal/operators";



@Injectable()
export class IntercomService {

    baseURL: string;

    constructor(private http: HttpClient) {
        this.baseURL = environment.baseURL + '/intercom/api';
    }

  sayText(text: string): Observable<any> {
    const scope = 'r1111';
    const volume = 65000;
    const url = `${this.baseURL}/saytext?scope=${scope}&volume=${volume}&text=${text}`;
    return this.http
      .get<any>(url)
      .pipe(catchError(this.handleError));
  }

  getLog(): Observable<any> {
    const url = `${this.baseURL}/log`;
    return this.http
      .get<any>(url)
      .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
      if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', err.error.message);
      } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          // let et = JSON.stringify(err.error);
          console.error(`Backend returned code ${err.status}, body was: ${err.message}`);
      }
      return throwError(err);
  }
}
