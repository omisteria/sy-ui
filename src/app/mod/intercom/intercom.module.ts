import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../shared.module";
import {MaterialModule} from "../material.module";
import {MainComponent} from "./pages/main/main.component";
import {IntercomService} from "./shared/intercom.service";

const routes: Routes = [
    {
        path: 'intercom',
        pathMatch: 'full',
        redirectTo: 'intercom/main'
    },
    {
        path     : 'intercom/main',
        component: MainComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        SharedModule,
        MaterialModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
      MainComponent
    ],
    exports: [
        MaterialModule,
        SharedModule
    ],
    providers: [
        IntercomService
    ]
})
export class IntercomModule {

}
