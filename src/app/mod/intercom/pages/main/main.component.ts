import { Component, OnInit } from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';
import {IntercomService} from "../../shared/intercom.service";



@Component({
    selector: 'sy-intercom-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  logMessages: any[] = [];
  text: string;

    constructor(
        private intercomService: IntercomService,
        public snackBar: MatSnackBar) {

    }

    ngOnInit(): void {

      setInterval(() => {
        this.intercomService.getLog()
          .subscribe(
            data => {
              this.logMessages = data;
            }
          )

      }, 3000);
    }

  clickSend() {
    if (this.text) {
      this.intercomService.sayText(this.text)
        .subscribe(
          d => {
            this.text = '';
            this.snackBar.open('Say Text', 'Success', <MatSnackBarConfig>{
              duration: 2000
            });
          },
          e => {
            this.snackBar.open(e, 'Error', <MatSnackBarConfig>{
              duration: 2000
            });
          }
        );
    }
  }

}
