import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {NavbarService} from "./shared/navbar.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    NavbarService
  ]
})
export class UiModule { }
