import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule, Routes} from "@angular/router";
import {StickerModule} from "./mod/sticker/sticker.module";
import {CommunicatorModule} from "./mod/communicator/communicator.module";
import {MaterialModule} from "./mod/material.module";
import {PersonsModule} from "./mod/persons/persons.module";
import {DevMonitorModule} from "./mod/devmonitor/devmonitor.module";
import {UiModule} from "./ui/ui.module";
import {IntercomModule} from "./mod/intercom/intercom.module";
import {FlexLayoutModule} from "@angular/flex-layout";

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'sticker/list'
  },
  {
    path     : 'sticker',
    loadChildren: './mod/sticker/sticker.module#StickerModule'
  },
  {
    path     : 'communicator',
    loadChildren: './mod/communicator/communicator.module#CommunicatorModule'
  },
  {
    path     : 'devmonitor',
    loadChildren: './mod/devmonitor/devmonitor.module#DevMonitorModule'
  },
  {
    path     : 'persons',
    loadChildren: './mod/persons/persons.module#PersonsModule'
  },
  {
    path     : 'intercom',
    loadChildren: './mod/intercom/intercom.module#IntercomModule'
  }
];

@NgModule({
    declarations: [
      AppComponent
    ],
    imports: [
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      CommonModule,
      BrowserAnimationsModule,
      HttpClientModule,
      CommunicatorModule,
      DevMonitorModule,
      PersonsModule,
      StickerModule,
      IntercomModule,
      UiModule,
      RouterModule.forRoot(routes)
    ],
    exports: [
      FormsModule,
      ReactiveFormsModule,
      StickerModule
    ],
    entryComponents: [

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
